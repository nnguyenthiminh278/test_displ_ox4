
import numpy as np
import sys

# ----------------------------------------------------------------------------------------
# ------- This script is used to get the coordinates from Gaussian (.fchk file) ----------
# -------------- and convert to matrix 1xm where m is number of atoms --------------------
# type: python3 Get_coord_1.py Ox4_GS.fchk GS.txt 
# type: python3 Get_coord_1.py Ox4_ES.fchk ES.txt 
# ----------------------------------------------------------------------------------------

# -------------- Find "Current cartesian coordinates" in .fhck file ----------------------
def getrawdata(infile):
    items = 0
    countlines = 0
    hessian = []                                            # save found data in this list
    f = open(infile, 'r')                                   # read input file
    found = False
    for line in f:
        line = line.rstrip('\n')                            # only the rightmost \n will be removed, leaving all the other untouched
        if found == True:
            data = list(filter(None, line.split(' ')))
            for n in range(0, len(data)):
                hessian += [float(data[n])]
                countlines += 1

        if "Current cartesian coordinates" in line:
            found = True
            data = list(filter(None, line.split(' ')))
            items = int(data[data.index("N=")+1])

        if countlines == items:
            found = False

    f.close()

    return hessian

# ------------ Convert the obtained data to raw matrix -----------------------

def savecoord(rawdata, outfile):
    m = len(rawdata)
    M = np.reshape(rawdata,(1, m)) 
# --------------------------------------------
    np.savetxt(outfile, M, delimiter=" ")

if __name__ == "__main__":
    infile = sys.argv[1]
    outfile = sys.argv[2]
    rawdata = getrawdata(infile)
    savecoord(rawdata, outfile)