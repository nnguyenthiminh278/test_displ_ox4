import numpy as np
import math
import sys

# ---------------------------------------------------------------------------------------
# -------------- This script is used to get the difference of coordinates ---------------
# ------------------------------ between GS and ES --------------------------------------
# type: python3 Subtract_2.py displ.txt Molecule.mol
# ---------------------------------------------------------------------------------------

# Open a file
line1 = []
with open('GS.txt', 'r') as f_GS:
    for line in f_GS.readlines():
        for num in line.split(' '):
            line1.append(float(num))
    #print("Coordinates of ground state: %s" %(line1))

line2 = []
with open('ES.txt', 'r') as f_ES:
    for line in f_ES.readlines():
        for num in line.split(' '):
            line2.append(float(num))
    #print("Coordinates of excited state: %s" % (line2))

result=[]
result = [line1_i - line2_i for line1_i, line2_i in zip(line1, line2)]

# ------------ Save the obtained subtraction to raw matrix then normalize  -----------------------

def savecoord(result, outfile, infile):
    # read the order of atoms from Molecule.mol file
    n_atoms = 44 # Number of atoms, CHANGE FOR OTHER MOLECULE
    n_coord = n_atoms*3
    ordering = []
    with open(infile) as reader:
        for i in range(5): reader.readline() #skip first 5 lines in Molecule.mol
        for i in range(n_atoms):
            ordering.extend([reader.readline().strip().split()[0]]*3)
    
    #mass of each atom, CHANGE FOR OTHER MOLECULE
    mass_C = math.sqrt(12.0000000* 1822.88848) # mass of C in au
    mass_N = math.sqrt(14.0030740* 1822.88848) # mass of N in au
    mass_O = math.sqrt(15.9949146 * 1822.88848) # mass of O in au
    mass_H = math.sqrt(1.00782504 * 1822.88848) # mass of H in au
    
    coef = dict(C=mass_C, N=mass_N, H=mass_H, O=mass_O) #CHANGE FOR OTHER MOLECULE

    M = np.reshape(result,(1, n_coord)) #save to matric 1xm
    #Get mass-weighted matrix
    for i in range(n_coord):
        M[0,i] *= coef[ordering[i]]

    np.savetxt(outfile, M, delimiter=" ")

# Close opend file
f_GS.close()
f_ES.close()

if __name__ == "__main__":
    outfile, infile = sys.argv[1], sys.argv[2]
    savecoord(result, outfile, infile)