
import numpy as np
import math
import sys


# -----------------------------------------------------------------------------------------------------------
# ----- This script is used to get coordinates of normal modes of ES from Gaussian (.fchk file) -------------
# -------------- and convert to the matrix 1 x 3N where N is number of atoms --------------------------------
# - type: python3 Get_nm_3.py Ox4_ES.fchk nm_ES.txt Molecule.mol---------------------------------------------------------
# -------------- Find "Vib-Modes" in .fhck file -------------------------------------------------------------

def getrawdata(infile):
    items = 0
    countlines = 0
    hessian = []                                            # save found data in this list
    f = open(infile, 'r')                                   # read input file
    found = False
    for line in f:
        line = line.rstrip('\n')                            # only the rightmost \n will be removed, leaving all the other untouched
        if found == True:
            data = list(filter(None, line.split(' ')))
            for n in range(0, len(data)):
                hessian += [float(data[n])]
                countlines += 1

        if "Vib-Modes" in line:
            found = True
            data = list(filter(None, line.split(' ')))
            items = int(data[data.index("N=")+1])

        if countlines == items:
            found = False

    f.close()

    return hessian

# ------------ Convert the obtained data to a matrix ------------------------------------------

def savecoord(rawdata, outfile, infile):
    #m = len(rawdata)
    n_atoms = 44      # Number of atoms, CHANGE FOR OTHER MOLECULE
    n_coord = n_atoms*3
    nm = n_atoms*3 - 6 # number of normal modes: 3N-6 or 3N-5
    M = np.reshape(rawdata,(nm, n_coord)) # save the extracted coords in matrix
    # read the order of atoms from Molecule.mol file
    ordering = []
    with open(infile) as reader:
        for i in range(5): reader.readline()
        for i in range(n_atoms):
            ordering.extend([reader.readline().strip().split()[0]]*3)
    
    # mass of each atom, CHANGE FOR OTHER MOLECULE
    mass_C = math.sqrt(12.0000000* 1822.88848) # mass of C in au
    mass_N = math.sqrt(14.0030740* 1822.88848) # mass of N in au
    mass_O = math.sqrt(15.9949146 * 1822.88848) # mass of O in au
    mass_H = math.sqrt(1.00782504 * 1822.88848) # mass of H in au
    
    coef = dict(C=mass_C, N=mass_N, H=mass_H, O=mass_O) #CHANGE FOR OTHER MOLECULE

    # Get mass-weighted matrix
    for i in range(n_coord):
        M[:,i] *= coef[ordering[i]]
    # Normalization
    M_norm = np.linalg.norm(M, axis=-1, keepdims=True)
    M = M / M_norm 
    #print("After normalization:") 
    #print(M)  
    np.savetxt(outfile, M, delimiter=" ")


# To check the othorgonalization: the off-diagonals of matrix A should be 0 and diagonal terms = 1 :
#    N = M.T
#    print("Check othornormal:")
#    A = M@N 
#    print(A)
#----------------------------------------------
   
if __name__ == "__main__":
    infile1 = sys.argv[1]
    outfile = sys.argv[2]
    infile2 = sys.argv[3]
    rawdata = getrawdata(infile1)
    savecoord(rawdata, outfile, infile2)
