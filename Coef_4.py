import numpy as np
import math
import sys

# -------------------------------------------------------------------------
# ----- This script is used to solve the linear combination ---------------
# -------------- and get the coefficients which are -----------------------
# type: python3 Coef_4.py outfile.txt
# -------------------------------------------------------------------------
cm1_au = 4.5563353e-6      # cm**-1 to au
cm1_au = float(cm1_au)

# open the normal mode coords and save into a list of linear combination vectors
my_list= []
n_atoms = 44      # Number of atoms CHANGE FOR OTHER MOLECULE
n_coord = n_atoms*3
nm = n_atoms*3 - 6 # number of normal modes: 3N-6 or 3N-5

with open('nm_ES.txt', 'r') as f_nm:
    for line in f_nm.readlines():
        for num in line.split(' '):
            my_list.append(float(num))
    A = np.array(my_list)
    A.resize(nm,n_coord)
    B = A.T

# Open a coord file of displacement
y = []
with open('displ.txt', 'r') as f_dif:
    for line in f_dif.readlines():
        for num in line.split(' '):
            y.append(float(num))
y = np.array(y)


# Get wj from ES.log file
def get_wj(infile):
    wj= []
    f = open(infile, 'r')
    for line in f:
        line = line.rstrip('\n')
        if "Frequencies --" in line:
            w = line.split(" ")
            for w in line.split(" "):
                s = w.strip()
                try:
                    wj.append(float(s))
                except ValueError:
                    pass
    f.close()
    return np.array(wj)
freq = get_wj('Ox4_ES.log') #CHANGE FOR OTHER MOLECULE

#Calculate the coeficients
def savecoord(wj,outfile):
    scalars = y@B
    result = np.abs(scalars * np.sqrt(freq*cm1_au))
    order = sorted(range(len(result)), key= lambda x: result[x], reverse=True)
    output = np.concatenate((result[order].reshape(-1,1), freq[order].reshape(-1,1)), axis=1)
    np.savetxt(outfile, output, delimiter=" ")

if __name__ == "__main__":
    outfile = sys.argv[1]
    savecoord(freq,outfile)
